Vigilant Media Central Administration
=====================================

The Viglant Media Central Administration site has been split into individual components.

Musicwhore.org Artist Database Administration can be found at http://admin.musicwhore.org/

Observant Records Artist Administration can be found at http://admin.observantrecords.com/

Duran Duran Networks Administration can be found at http://duran-duran.net/admin/

Austin Stories has been shut down, and its administrative interface will not be migrated.

Souce code for the administration site can be downloaded by tag.
